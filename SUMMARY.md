# Summary

## Introduction

* [Introduction](README.md)
* [Keep format](keep-format.md)
  * [test](keep-format/test.md)

## Think Python

* [Chapters](ThinkPython/think-python-chapters.md)
  * [Strings](ThinkPython/strings.md)
  * [Lists](ThinkPython/lists.md)
  * [Dictionaries](ThinkPython/dictionaries.md)
  * [Classes](ThinkPython/classes.md)
* [Assignments](ThinkPython/assignments.md)
  * [Chapter 3](ThinkPython/assignments/chapter-3.md)
  * [Chapter 4](ThinkPython/assignments/chapter-4.md)
  * [Chapter 5](ThinkPython/assignments/chapter-5.md)
  * [Chapter 6](ThinkPython/assignments/chapter-6.md)
  * [Chapter 7](ThinkPython/assignments/chapter-7.md)
  * [Chapter 8](ThinkPython/assignments/chapter-8.md)
  * [Chapter 9](ThinkPython/assignments/chapter-9.md)
  * [Chapter 10](ThinkPython/assignments/chapter-10.md)
  * [Chapter 11](ThinkPython/assignments/chapter-11.md)
  * [Chapter 13](ThinkPython/assignments/chapter-13.md)
* [Studios](ThinkPython/studios.md)
  * [1. Holiday](ThinkPython/studios/holiday.md)
  * [2. Donuts](ThinkPython/studios/donuts.md)
  * [3. Turtle Racing](ThinkPython/studios/turtle.md)
  * [4. Wagon Wheel](ThinkPython/studios/wagon.md)
  * [5. Blurring](ThinkPython/studios/blurr.md)
  * [6. Sorted](ThinkPython/studios/sorted.md)
  * [7. Bugz](ThinkPython/studios/bugz.md)
  * [8. Bubble Sort](ThinkPython/studios/bubble.md)
  * [9. Yahtzee](ThinkPython/studios/yahtzee.md)
  * [10. Counting Characters](ThinkPython/studios/chars.md)
  * [11. Class Design](ThinkPython/studios/class.md)
  * [12. Blog Design](ThinkPython/studios/blog.md)
* [Other](ThinkPython/other.md)
  * [Initials](ThinkPython/other/initials.md)
  * [Crypto](ThinkPython/other/crypto.md)
  * [Math](ThinkPython/other/math.md)

