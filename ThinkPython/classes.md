# List chapter 10
[Link to ThinkPython](https://runestone.launchcode.org/runestone/static/thinkcspy/ClassesBasics/Exercises.html)

{% method %}
## Exercise 1

Add a `distanceFromPoint` method that works similar to `distanceFromOrigin` except that it takes a Point as a parameter and computes the distance between that point and self.

{% sample lang="python" %}

```python
class Point:
    def __init__(self, initX, initY):
        self.x = initX
        self.y = initY
    
    def getY(self):
        return self.y
    
    def getX(self):
        return self.x
    
    def distanceFromOrigin(self):
        return ((self.x ** 2) + (self.y **2) ** 0.5)
    
    def distanceFromPoint(self, target):
        xDiff = self.x - target.getX()
        yDiff = self.y - target.getY()
        return ((xDiff ** 2) + (yDiff **2) ** 0.5)

    def __str__(self):
        return "x= " + str(self.x) + ", y= " + str(self.y)

a = Point(2,4)
b = Point(7,7)

print(a.distanceFromPoint(b))
print(b.distanceFromPoint(a))

>>> 28.0
>>> 28.0

```
{% endmethod %}

{% method %}
## Exercise 2

Add a method reflect_x to the class Point which returns a new Point, one which is the reflection of the point across the x-axis. For example, Point(3, 5).reflect_x() is (3, -5)

{% sample lang="python" %}
```python
class Point:
    def __init__(self, initX, initY):
        self.x = initX
        self.y = initY
    
    def getY(self):
        return self.y
    
    def getX(self):
        return self.x
    
    def distanceFromOrigin(self):
        return ((self.x ** 2) + (self.y **2) ** 0.5)
    
    def distanceFromPoint(self, target):
        xDiff = self.x - target.getX()
        yDiff = self.y - target.getY()
        return ((xDiff ** 2) + (yDiff **2) ** 0.5)
    
    def reflect_x(self):
        y = -(self.y)
        x = self.x
        return Point(x, y)
    
    def __str__(self):
        return "x= " + str(self.x) + ", y= " + str(self.y)

a = Point(2,4)
b = Point(7,7)

print(a.reflect_x())
print(b.reflect_x())

>>> x= 2, y= -4
>>> x= 7, y= -7

```
{% endmethod %}

{% method %}

## Exercise 3
The equation of a straight line is `y = ax + b`, (or perhaps `y = mx + c`). The coefficients a and b completely describe the line. Write a method in the Point class so that if a point instance is given another point, it will compute the equation of the straight line joining the two points. It must return the two coefficients as a tuple of two values. For example:

```
>>> print(Point(4, 11).get_line_to(Point(6, 15)))
>>> (2, 3)
```

This tells us that the equation of the line joining the two points is “y = 2x + 3”. When will your method fail?


{% sample lang="python" %}
```python
    def get_line_to(self, target):
        x1 = self.x
        y1 = self.y
        x2 = target.getX()
        y2 = target.getY()
        a = (y1 - y2) / (x1 - x2)
        b = y1 - (x1 * a)
        return (a, b)
```
Will fail on anything that creates division by 0 when x1 == x2.
{% endmethod %}

{% method %}

## Exercise 4
Add a method called move that will take two parameters, call them dx and dy. The method will cause the point to move in the x and y direction the number of units given. (Hint: you will change the values of the state of the point)


{% sample lang="python" %}
```python
def move(self, dx, dy):
    self.x += dx
    self.y += dy
```
{% endmethod %}

{% method %}

## Exercise 5
Given three points that fall on the circumference of a circle, find the center and radius of the circle.

https://blancosilva.github.io/post/2014/10/28/Computational-Geometry-in-Python.html 

{% sample lang="python" %}
```python

```
{% endmethod %}

# Chapter 14
{% method %}

## Exercise 1
We can represent a rectangle by knowing three things: the location of its lower left corner, its width, and its height. Create a class definition for a `Rectangle` class using this idea. To create a Rectangle object at location (4,5) with width 6 and height 5, we would do the following: `r = Rectangle(Point(4, 5), 6, 5)`

{% sample lang="python" %}
```python
class Point():
    def __init__(self, x,y):
        self.x = x
        self.y = y
    
    def getX(self):
        return self.x
    
    def getY(self):
        return self.y
    
    def __str__(self):
        return "x=" + str(self.x) + ", y=" + str(self.y)
    
class Rectangle():
    def __self__(self, initPosition, initWidth, initHeight):
        self.width = initWidth
        self.height = initHeight
        self.position = initPosition

r = Rectangle(Point(4, 5), 6, 5)
print(r)
```
{% endmethod %}
{% method %}
## Exercise 2
Add the following accessor methods to the Rectangle class: `getWidth`, `getHeight`, `__str__`.

{% sample lang="python" %}
```python
class Point:
    def __init__(self, x,y):
        self.x = x
        self.y = y
    
    def getX(self):
        return self.x
    
    def getY(self):
        return self.y
    
    def __str__(self):
        return "x=" + str(self.x) + ", y=" + str(self.y)
    
class Rectangle:
    def __init__(self, initPosition, initWidth, initHeight):
        self.width = initWidth
        self.height = initHeight
        self.position = initPosition
    
    def getWidth(self):
        return self.width
    
    def getHeight(self):
        return self.height

    def __str__(self):
        return "width: " + str(self.width) + ", height: " + str(self.height) + ", position: " + str(self.position)
    
r = Rectangle(Point(4, 5), 6, 5)
print(r)

>>> width: 6, height: 5, position: x=4, y=5
```
{% endmethod %}

{% method %}
## Exercise 3
Add a method area to the Rectangle class that returns the area of any instance: `testEqual(r.area(), 50)`

{% sample lang="python" %}
```python   
(...)
def area(self):
    return self.width * self.height
    
r = Rectangle(Point(4, 5), 6, 5)
print(r)
print(r.area())

```
{% endmethod %}

{% method %}
## Exercise 4
Write a perimeter method in the Rectangle class so that we can find the perimeter of any rectangle instance:

{% sample lang="python" %}
```python   
def perimeter(self):
    return self.width*2 + self.height*2
    
r = Rectangle(Point(4, 5), 6, 5)
print(r.perimeter())

>>> 22
```
{% endmethod %}

{% method %}
## Exercise 5
Write a transpose method in the Rectangle class that swaps the width and the height of any rectangle instance:

{% sample lang="python" %}
```python   
(...)
def transpose(self):
    (self.width, self.height) = (self.height, self.width)
    
r = Rectangle(Point(100, 50), 10, 5)
print(r.width)
print(r.height)
r.transpose()
print(r.width)
print(r.height)

>>> 6
>>> 5
>>> 5
>>> 6
```
{% endmethod %}

{% method %}

## Exercise 6

Write a new method in the Rectangle class to test if a Point falls within the rectangle. For this exercise, assume that a rectangle at (0,0) with width 10 and height 5 has open upper bounds on the width and height, i.e. it stretches in the x direction from [0 to 10), where 0 is included but 10 is excluded, and from [0 to 5) in the y direction. So it does not contain the point (10, 2). These tests should pass:

{% sample lang="python" %}
```python   
def contains(self, point):
    x = 0 <= point.x < self.width
    y = 0 <= point.y < self.height
    return x and y

testEqual(r.contains(Point(0, 0)), True)
testEqual(r.contains(Point(3, 3)), True)
testEqual(r.contains(Point(3, 7)), False)
testEqual(r.contains(Point(3, 5)), False)
testEqual(r.contains(Point(3, 4.99999)), True)
testEqual(r.contains(Point(-3, -3)), False)
```
{% endmethod %}

{% method %}

## Exercise 7

Write a new method called diagonal that will return the length of the diagonal that runs from the lower left corner to the opposite corner.

{% sample lang="python" %}
```python   
def diagonal(self):
    return (self.width**2 + self.height**2) ** 0.5
```
{% endmethod %}

{% method %}

## Exercise 8

In games, we often put a rectangular “bounding box” around our sprites in the game. We can then do collision detection between, say, bombs and spaceships, by comparing whether their rectangles overlap anywhere.

Write a function to determine whether two rectangles collide. Hint: this might be quite a tough exercise! Think carefully about all the cases before you code.

{% sample lang="python" %}
```python   
class Point():
    def __init__(self, x,y):
        self.x = x
        self.y = y
    
    def getX(self):
        return self.x
    
    def getY(self):
        return self.y
    
    def __str__(self):
        return "x=" + str(self.x) + ", y=" + str(self.y)
    
class Rectangle():
    def __init__(self, initPosition, initWidth, initHeight):
        self.width = initWidth
        self.height = initHeight
        self.position = initPosition
    
    def getWidth(self):
        return self.width
    
    def getHeight(self):
        return self.height
    
    def area(self):
        return self.width * self.height

    def perimeter(self):
        return self.width*2 + self.height*2
    
    def transpose(self):
        (self.width, self.height) = (self.height, self.width)

    def contains(self, point):
        x = self.position.getX() <= point.x < self.width
        y = self.position.getY() <= point.y < self.height
        return x and y

    def collideWith(self, otherRec):
        a = otherRec.position
        b = Point(otherRec.getWidth() + self.width, otherRec.getHeight())
        c = Point(otherRec.getWidth() + self.width, otherRec.getHeight() + self.height)
        d = Point(otherRec.getWidth(), otherRec.getHeight() + self.height)
        otherInSelf = self.contains(a) or self.contains(b) or self.contains(c) or self.contains(d)

        w = self.position
        x = Point(self.getWidth() + otherRec.width, self.getHeight())
        y = Point(self.getWidth() + otherRec.width, self.getHeight() + otherRec.height)
        z = Point(self.getWidth(), self.getHeight() + otherRec.height)
        selfInOther = otherRec.contains(a) or otherRec.contains(b) or otherRec.contains(c) or otherRec.contains(d)

        return otherInSelf or selfInOther
    
    def __str__(self):
        return "width: " + str(self.width) + ", height: " + str(self.height) + ", position: " + str(self.position)
    
r1 = Rectangle(Point(4, 5), 6, 5)
r2 = Rectangle(Point(0, 0), 10, 10)

print(r2.collideWith(r1))
print(r1.collideWith(r2))
```
{% endmethod %}



