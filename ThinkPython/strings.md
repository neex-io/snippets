# String chapter exercises
[Link to ThinkPython](https://runestone.launchcode.org/runestone/static/thinkcspy/StringsContinued/Exercises.html)

{% method %}
## Exercise 1

In Robert McCloskey’s book Make Way for Ducklings, the names of the ducklings are Jack, Kack, Lack, Mack, Nack, Ouack, Pack, and Quack. This loop tries to output these names in order.

```python
prefixes = "JKLMNOPQ"
suffix = "ack"

for p in prefixes:
print(p + suffix)
```

Of course, that’s not quite right because Ouack and Quack are misspelled. Can you fix it?



{% sample lang="python" %}

```python
prefixes = "JKLMNOPQ"
suffix = "ack"

for p in prefixes:
if p in "QO":
print(p + "u" + suffix)
else:
print(p + suffix)
```

{% endmethod %}

{% method %}

## Exercise 2
Print out a neatly formatted multiplication table, up to 12 x 12. Your output should look something like this:

```python
1 2 3 4 5 6 7 8 9 10 11 12
2 4 6 8 10 12 14 16 18 20 22 24
3 6 9 12 15 18 21 24 27 30 33 36
4 8 12 16 20 24 28 32 36 40 44 48
5 10 15 20 25 30 35 40 45 50 55 60
6 12 18 24 30 36 42 48 54 60 66 72
7 14 21 28 35 42 49 56 63 70 77 84
8 16 24 32 40 48 56 64 72 80 88 96
9 18 27 36 45 54 63 72 81 90 99 108
10 20 30 40 50 60 70 80 90 100 110 120
11 22 33 44 55 66 77 88 99 110 121 132
12 24 36 48 60 72 84 96 108 120 132 144
```

{% sample lang="python" %}


```python
def printMulti(number):
for num in range(1, number+1):
line = ""
for position in range(1, number):
line += str(num * position) + "\t"
line += str(num * number) # so the last line does not have tab character
print(line)

>>>printMulti(12)
>>>print("")
>>>printMulti(5)
```

{% endmethod %}
{% method %}
## Exercise 3
Write a function reverse that receives a string argument, and returns a reversed version of the string.

{% sample lang="python" %}


```python
from test import testEqual

def reverse(text):
reversed = ""
for letter in text:
reversed = letter + reversed
return reversed

testEqual(reverse("happy"), "yppah")
testEqual(reverse("Python"), "nohtyP")
testEqual(reverse(""), "")
```

{% endmethod %}


{% method %}
## Exercise 4
Write a function that recognizes palindromes. (Hint: use your reverse function to make this easy!).



{% sample lang="python" %}


```python
from test import testEqual

def reverse(text):
reversed = ""
for letter in text:
reversed = letter + reversed
return reversed

def is_palindrome(text):
return text == reverse(text)

testEqual(is_palindrome('abba'), True)
testEqual(is_palindrome('abab'), False)
testEqual(is_palindrome('straw warts'), True)
testEqual(is_palindrome('a'), True)
testEqual(is_palindrome(''), True)
```

{% endmethod %}

{% method %}
## Exercise 5
Write a function that removes the first occurrence of a string from another string.

{% sample lang="python" %}


```python
from test import testEqual

def remove(substr,theStr):
    start = theStr.find(substr)

    if start != -1:
        stop = start + len(substr)
        theStr = theStr[0:start] + theStr[stop:]
        
    return theStr

testEqual(remove('an', 'banana'), 'bana')
testEqual(remove('cyc', 'bicycle'), 'bile')
testEqual(remove('iss', 'Mississippi'), 'Missippi')
testEqual(remove('egg', 'bicycle'), 'bicycle')
```

{% endmethod %}

{% method %}
## Exercise 6
Write a function that removes all occurrences of a string from another string.

{% sample lang="python" %}


```python
from test import testEqual

def remove(substr,theStr):
    start = theStr.find(substr)

    if start != -1:
        stop = start + len(substr)
        theStr = theStr[0:start] + theStr[stop:]
        
    return theStr

def remove_all(substr,theStr):
    isThere = theStr.find(substr)
    while isThere > -1:
        theStr = remove(substr,theStr)
        isThere = theStr.find(substr)
    return theStr

testEqual(remove_all('an', 'banana'), 'ba')
testEqual(remove_all('cyc', 'bicycle'), 'bile')
testEqual(remove_all('iss', 'Mississippi'), 'Mippi')
testEqual(remove_all('eggs', 'bicycle'), 'bicycle')
```
{% endmethod %}

{% method %}
## Exercise 7
Write a function that implements a substitution cipher. In a substitution cipher one letter is substituted for another to garble the message. For example A -> Q, B -> T, C -> G etc. your function should take two parameters, the message you want to encrypt, and a string that represents the mapping of the 26 letters in the alphabet. Your function should return a string that is the encrypted version of the message.

{% sample lang="python" %}
Solution based on character codes:

```python
def encode(string):
    encoded = ""
    for letter in string:
        position = ord(letter)
        encoded += chr(position + 15)
    return encoded
```
Solution with randomly generated cipher:

```python
import string
import random

def make_cipher():
    alphabet = string.ascii_lowercase + " "
    list_alphabet = list(alphabet)
    random.shuffle(list_alphabet)
    cipher = ''.join(list_alphabet)
    print(cipher)
    return cipher

def encode(str, cipher):
    alphabet = string.ascii_lowercase + " "
    encoded = ""
    for letter in str:
        position = alphabet.find(letter)
        encoded += cipher[position]
    return encoded

cipher = make_cipher()
print(encode("hello", cipher))

>>> rqhacdzygnxlpvtjukfeimoswb
>>> ycllt
>>>
>>> dvlrbfsmxipeznuohcqyajwkgt
>>> mbeeu
```

{% endmethod %}

{% method %}
## Exercise 8
Write a function that decrypts the message from the previous exercise. It should also take two parameters. The encrypted message, and the mixed up alphabet. The function should return a string that is the same as the original unencrypted message.


{% sample lang="python" %}


```python
import string
import random

def make_cipher():
    alphabet = string.ascii_lowercase + " "
    list_alphabet = list(alphabet)
    random.shuffle(list_alphabet)
    cipher = ''.join(list_alphabet)
    print(cipher)
    return cipher

def decode(str, cipher):
    alphabet = string.ascii_lowercase + " "
    encoded = str
    decoded = ""
    for letter in encoded:
        position = cipher.find(letter)
        decoded += alphabet[position]
    return decoded

# take data from last exercise
print(decode("ycllt", "rqhacdzygnxlpvtjukfeimoswb"))
print(decode("mbeeu", "dvlrbfsmxipeznuohcqyajwkgt"))

>>> hello
>>> hello
```

{% endmethod %}

{% method %}
## Exercise 9
Write a function called removeDups that takes a string and creates a new string by only adding those characters that are not already present. In other words, there will never be a duplicate letter added to the new string.


{% sample lang="python" %}


```python
def removeDups(astring):
    clean = ""
    for letter in astring:
        if letter not in clean:
            clean += letter
    return clean

print(removeDups("mississippi"))   #should print misp

```

{% endmethod %}

{% method %}
## Exercise 10
Write a function called rot13 that uses the Caesar cipher to encrypt a message. The Caesar cipher works like a substitution cipher but each character is replaced by the character 13 characters to ‘its right’ in the alphabet. So for example the letter a becomes the letter n. If a letter is past the middle of the alphabet then the counting wraps around to the letter a again, so n becomes a, o becomes b and so on. Hint: Whenever you talk about things wrapping around its a good idea to think of modulo arithmetic.


{% sample lang="python" %}


```python
import string
def rot13(mess):
    alphabet = string.ascii_lowercase
    encoded = ""
    for letter in mess:
        if letter == " ":
            encoded += " "
        else:
            position = (alphabet.find(letter) + 13) % 26
            encoded += alphabet[position]
    return encoded

print(rot13('abcde'))
print(rot13('nopqr'))
print(rot13(rot13('since rot thirteen is symmetric you should see this message')))
```

{% endmethod %}





