# List chapter 10
[Link to ThinkPython](https://runestone.launchcode.org/runestone/static/thinkcspy/Dictionaries/Exercises.html)

{% method %}
## Exercise 1

Write a program that allows the user to enter a string. It then prints a table of the letters of the alphabet in alphabetical order which occur in the string together with the number of times each letter occurs. Case should be ignored. A sample run of the program might look this this:

{% sample lang="python" %}

```python
import string

def letterCount(text):
    alphabet = string.ascii_lowercase
    text = text.lower()
    
    letters = {}
    
    for letter in text:
        if letter in alphabet:
            if letters.get(letter):
                letters[letter] += 1
            else:
                letters[letter] = 1
    keys = letters.keys()
    keys.sort()
    
    for key in keys:
        print(key, letters[key])

letterCount("Abcde fg ggfd x i !")
```
{% endmethod %}

{% method %}
## Exercise 2

Give the Python interpreter’s response to each of the following from a continuous interpreter session:

```python
>>> d = {'apples': 15, 'bananas': 35, 'grapes': 12}
>>> d['bananas']
35
>>> d['oranges'] = 20
>>> len(d)
4
>>> 'grapes' in d
True
>>> d['pears']
Traceback (most recent call last):
  File "<pyshell#9>", line 1, in <module>
    d['pears']
KeyError: 'pears'
>>> d.get('pears', 0)
0
>>> fruits = d.keys()
>>> sorted(fruits)
['apples', 'bananas', 'grapes', 'oranges']
>>> print(fruits)
dict_keys(['bananas', 'oranges', 'apples', 'grapes'])
>>> del d['apples']
>>> 'apples' in d
False

# make these tests work...
# new_inventory = {}
# add_fruit(new_inventory, 'strawberries', 10)
# testEqual('strawberries' in new_inventory, True)
# testEqual(new_inventory['strawberries'], 10)
# add_fruit(new_inventory, 'strawberries', 25)
# testEqual(new_inventory['strawberries'] , 35)
```


{% sample lang="python" %}

```python
from test import testEqual

def add_fruit(inventory, fruit, quantity=0):
    value = inventory.get(fruit, 0)
    inventory[fruit] = value + quantity

# make these tests work...
new_inventory = {}
add_fruit(new_inventory, 'strawberries', 10)
testEqual('strawberries' in new_inventory, True)
testEqual(new_inventory['strawberries'], 10)
add_fruit(new_inventory, 'strawberries', 25)
testEqual(new_inventory['strawberries'] , 35)
```
{% endmethod %}

{% method %}
## Exercise 3

Here’s a table of English to Pirate translations:

| English | Pirate |
| :--- | :--- |
| sir | matey |
| hotel | fleabag inn |
| student | swabbie |
| boy | matey |
| madam | proud beauty |
| professor | foul blaggart |
| restaurant | galley |
| your | yer |
| excuse | arr |
| students | swabbies |
| are | be |
| lawyer | foul blaggart |
| the | th' |
| restroom | head |
| my | me |
| hello | avast |
| is | be |
| man | matey |


{% sample lang="python" %}

```python
from test import testEqual
import string

def translate(text):
    slang_dictionary = {"sir": "matey", "hotel": "fleabag inn", "student": "swabbie", "boy": "matey", "madam": "proud beauty", "professor": "foul blaggart", "restaurant": "galley", "your": "yer", "excuse": "arr", "students": "swabbies", "are": "be", "lawyer": "foul blaggart", "the": "th'", "restroom": "head", "my": "me", "hello": "avast", "is": "be", "man": "matey"}
    text_list = text.split(" ")
    panctuation = string.punctuation
    slang_list = []
    
    for word in text_list:
        # check for panctuation
        keepFront = ""
        keepBack = ""

        if word[-1] in panctuation:
            keepBack = word[-1]
            word = word[:-1]
        if word[0] in panctuation:
            keepFront = word[0]
            word = word[1:]

        # translate
        if slang_dictionary.get(word, False):
            slang_list += [keepFront + slang_dictionary[word] + keepBack]
        else:
            slang_list += [keepFront + word + keepBack]

    slang_text = " ".join(slang_list)
    return slang_text

text = "hello my man, please excuse your professor to the restroom!"
testEqual(translate(text), "avast me matey, please arr yer foul blaggart to th' head!")

```
{% endmethod %}