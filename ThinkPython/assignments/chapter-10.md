## Weekly Graded Assignment (10)

{% method %}
Write a function to find the sum of all the even numbers in a list.

Normally we start you off by providing the function definition statement, e.g.:

```python
def launch_rockets(destination, num_passengers):
      # your code here
```

But in this case we will leave that to you! In other words, you will need to write that def line yourself. Make sure you give your function the name sum_evens, so that the tests work. Your function should accept one argument, the list of numbers to be summed.


{% sample lang="python" %}


```python
# TODO
# define a function called sum_evens, which receives one argument, a list of numbers.
# your function should return the sum of all the even numbers in the list
def sum_evens(list):
    sum = 0
    for number in list:
        if number%2 == 0:
            sum += number
    return sum

# don't copy these tests into Vocareum
from test import testEqual

testEqual(sum_evens([2,3,4]), 6)
testEqual(sum_evens([]), 0)
testEqual(sum_evens([0,7,2,4,2,1]), 8)
testEqual(sum_evens([0,1,2,3,4,5,6,7,8,9]), 20)
testEqual(sum_evens(range(200,500)), 52350)

```

{% endmethod %}




