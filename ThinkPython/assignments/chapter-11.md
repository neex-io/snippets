## Weekly Graded Assignment (11)

{% method %}
Write a function that will sum up all the elements in a list up to but not including the first even number.

{% sample lang="python" %}


```python
def sum_of_initial_odds(nums):
    sum = 0
    for num in nums:
        if num%2 == 0:
            return sum
        else:
            sum += num
    return sum

# don't include these tests in Vocareum
from test import testEqual

testEqual(sum_of_initial_odds([1,3,1,4,3,8]), 5)
testEqual(sum_of_initial_odds([6,1,3,5,7]), 0)
testEqual(sum_of_initial_odds([1, -7, 10, 23]), -6)
testEqual(sum_of_initial_odds(range(1,555,2)), 76729)
```

{% endmethod %}




