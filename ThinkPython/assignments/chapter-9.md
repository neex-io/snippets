## Weekly Graded Assignment (9)

{% method %}

Write a function that mirrors its argument. For example, `mirror('good')` should return a string holding the value `gooddoog`. (Hint: Make use of the reverse function that you wrote in exercise 3 above.)

There is one additional condition to consider: if the test string contains the lowercase letter “a”, then the mirror function should return a string that is ALL CAPS. For example, `mirror('abc')` should return a string holding the value `ABCCBA`.

{% sample lang="python" %}


```python
def mirror(text):
    if "a" in text:
        text = text.upper()
    return text + reverse(text)

def reverse(text):
    reversed = ""
    for letter in text:
        reversed = letter + reversed
    return reversed

# Don't copy these tests into Vocareum
from test import testEqual
testEqual(mirror('good'), 'gooddoog')
testEqual(mirror('Python'), 'PythonnohtyP')
testEqual(mirror(''), '')
testEqual(mirror('act'), 'ACTTCA')
```

{% endmethod %}




