## Random math stuff

{% method %}
### greatest common divisor

{% sample lang="python" %}
```python
def gcd(m, n):
    while m % n != 0:
        oldm = m
        oldn = n

        m = oldn
        n = oldm % oldn

    return n

print(gcd(12, 16))
```
{% endmethod %}

